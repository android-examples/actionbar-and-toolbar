package br.com.wxu.actionbarandtoolbar

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.Toolbar
import android.view.Menu
import android.view.MenuItem
import android.widget.Toast

class MainActivity : AppCompatActivity(), Toolbar.OnMenuItemClickListener {

    private val toolbar by lazy { findViewById<Toolbar>(R.id.toolbar) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        supportActionBar?.setHomeAsUpIndicator(R.mipmap.ic_launcher)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        toolbar.inflateMenu(R.menu.main_menu)
        toolbar.title = "Toolbar"
        toolbar.setNavigationIcon(R.mipmap.ic_launcher)
        toolbar.setOnMenuItemClickListener(this@MainActivity)
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.main_menu, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when(item?.itemId) {
            R.id.menu_send_message -> {
                Toast.makeText(this@MainActivity, "Send Message", Toast.LENGTH_LONG).show()
            }
            R.id.menu_settings -> {
                Toast.makeText(this@MainActivity, "Settings", Toast.LENGTH_LONG).show()
            }
            android.R.id.home -> {
                Toast.makeText(this@MainActivity, "Home", Toast.LENGTH_LONG).show()
            }
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onMenuItemClick(item: MenuItem?): Boolean {
        when(item?.itemId) {
            R.id.menu_send_message -> {
                Toast.makeText(this@MainActivity, "Send Message", Toast.LENGTH_LONG).show()
            }
            R.id.menu_settings -> {
                Toast.makeText(this@MainActivity, "Settings", Toast.LENGTH_LONG).show()
            }
            android.R.id.home -> {
                Toast.makeText(this@MainActivity, "Home", Toast.LENGTH_LONG).show()
            }
        }
        return false
    }
}
